﻿using Marshalls.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Marshalls.Domain.Interface
{
    public interface IEmployeeSalaryDomain
    {
        #region Synchronous Methods
        bool Insert(EmployeeSalary employeeSalary);
        bool Update(EmployeeSalary employeeSalary);
        bool Delete(int Id);
        EmployeeSalary Get(int Id);
        IEnumerable<EmployeeSalary> GetAll();
        #endregion

        #region Async Methods
        Task<bool> InsertAsync(EmployeeSalary employeeSalary);
        Task<bool> UpdateAsync(EmployeeSalary employeeSalary);
        Task<bool> DeleteAsync(int Id);
        Task<EmployeeSalary> GetAsync(int Id);
        Task<IEnumerable<EmployeeSalary>> GetAllAsync();
        #endregion
    }
}
