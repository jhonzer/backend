﻿using Marshalls.Transversal.Common;
using Marshalls.Application.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Marshalls.Application.Interface
{
    public interface IEmployeeSalaryApplication
    {
        #region Method Sync
        Response<bool> Insert(EmployeeSalaryDTO employeeSalaryDto);
        Response<bool> Update(EmployeeSalaryDTO employeeSalaryDto);
        Response<bool> Delete(int Id);
        Response<EmployeeSalaryDTO> Get(int Id);
        Response<IEnumerable<EmployeeSalaryDTO>> GetAll();
        #endregion

        #region Method Async
        Task<Response<bool>> InsertAsync(EmployeeSalaryDTO employeeSalaryDto);
        Task<Response<bool>> UpdateAsync(EmployeeSalaryDTO employeeSalaryDto);
        Task<Response<bool>> DeleteAsync(int Id);
        Task<Response<EmployeeSalaryDTO>> GetAsync(int Id);
        Task<Response<IEnumerable<EmployeeSalaryDTO>>> GetAllAsync();
        #endregion
    }
}
