﻿using AutoMapper;
using Marshalls.Application.Interface;
using Marshalls.Domain.Entity;
using Marshalls.Domain.Interface;
using Marshalls.Transversal.Common;
using Marshalls.Application.DTO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marshalls.Application.Main
{
    public class EmployeeSalaryApplication : IEmployeeSalaryApplication
    {
        private readonly IEmployeeSalaryDomain _employeeSalaryDomain;
        private readonly IMapper _mapper;

        public EmployeeSalaryApplication(IEmployeeSalaryDomain employeeSalaryDomain, IMapper mapper)
        {
            _employeeSalaryDomain = employeeSalaryDomain;
            _mapper = mapper;
        }

        #region Métodos Síncronos
        public Response<bool> Insert(EmployeeSalaryDTO employeeSalaryDto)
        {
            var response = new Response<bool>();
            try
            {
                var employeeSalary = _mapper.Map<EmployeeSalary>(employeeSalaryDto);
                response.Data = _employeeSalaryDomain.Insert(employeeSalary);
                if (response.Data)
                {
                    response.isSuccess = true;
                    response.Message = "Registro exitoso";
                }
            }
            catch (Exception e)
            {
                response.Message = e.Message;
            }
            return response;
        }
        public Response<bool> Update(EmployeeSalaryDTO employeeSalaryDto)
        {
            var response = new Response<bool>();
            try
            {
                var employeeSalary = _mapper.Map<EmployeeSalary>(employeeSalaryDto);
                response.Data = _employeeSalaryDomain.Update(employeeSalary);
                if (response.Data)
                {
                    response.isSuccess = true;
                    response.Message = "Actualización exitosa";
                }
            }
            catch (Exception e)
            {
                response.Message = e.Message;
            }
            return response;
        }
        public Response<bool> Delete(int Id)
        {
            var response = new Response<bool>();
            try
            {
                response.Data = _employeeSalaryDomain.Delete(Id);
                if (response.Data)
                {
                    response.isSuccess = true;
                    response.Message = "Eliminación exitosa";
                }
            }
            catch (Exception e)
            {
                response.Message = e.Message;
            }
            return response;
        }
        public Response<EmployeeSalaryDTO> Get(int Id)
        {
            var response = new Response<EmployeeSalaryDTO>();
            try
            {
                var employeeSalary = _employeeSalaryDomain.Get(Id);
                response.Data = _mapper.Map<EmployeeSalaryDTO>(employeeSalary);

                if (response.Data != null)
                {
                    response.isSuccess = true;
                    response.Message = "Consulta exitosa";
                }
            }
            catch (Exception e)
            {
                response.Message = e.Message;
            }
            return response;
        }
        public Response<IEnumerable<EmployeeSalaryDTO>> GetAll()
        {
            var response = new Response<IEnumerable<EmployeeSalaryDTO>>();
            try
            {
                var employeeSalary = _employeeSalaryDomain.GetAll();
                response.Data = _mapper.Map<IEnumerable<EmployeeSalaryDTO>>(employeeSalary);

                if (response.Data != null)
                {
                    response.isSuccess = true;
                    response.Message = "Consulta exitosa";
                }
            }
            catch (Exception e)
            {
                response.Message = e.Message;
            }
            return response;
        }
        #endregion

        #region Métodos Asíncronos
        public async Task<Response<bool>> InsertAsync(EmployeeSalaryDTO employeeSalaryDto)
        {
            var response = new Response<bool>();
            try
            {
                var employeeSalary = _mapper.Map<EmployeeSalary>(employeeSalaryDto);
                response.Data = await _employeeSalaryDomain.InsertAsync(employeeSalary);
                if (response.Data)
                {
                    response.isSuccess = true;
                    response.Message = "Registro exitoso";
                }
            }
            catch (Exception e)
            {
                response.Message = e.Message;
            }
            return response;
        }
        public async Task<Response<bool>> UpdateAsync(EmployeeSalaryDTO employeeSalaryDto)
        {
            var response = new Response<bool>();
            try
            {
                var employeeSalary = _mapper.Map<EmployeeSalary>(employeeSalaryDto);
                response.Data = await _employeeSalaryDomain.UpdateAsync(employeeSalary);
                if (response.Data)
                {
                    response.isSuccess = true;
                    response.Message = "Actualización exitosa";
                }
            }
            catch (Exception e)
            {
                response.Message = e.Message;
            }
            return response;
        }
        public async Task<Response<bool>> DeleteAsync(int Id)
        {
            var response = new Response<bool>();
            try
            {
                response.Data = await _employeeSalaryDomain.DeleteAsync(Id);
                if (response.Data)
                {
                    response.isSuccess = true;
                    response.Message = "Eliminación exitosa";
                }
            }
            catch (Exception e)
            {
                response.Message = e.Message;
            }
            return response;
        }
        public async Task<Response<EmployeeSalaryDTO>> GetAsync(int Id)
        {
            var response = new Response<EmployeeSalaryDTO>();
            try
            {
                var employeeSalary = await _employeeSalaryDomain.GetAsync(Id);
                response.Data = _mapper.Map<EmployeeSalaryDTO>(employeeSalary);

                if (response.Data != null)
                {
                    response.isSuccess = true;
                    response.Message = "Consulta exitosa";
                }
            }
            catch (Exception e)
            {
                response.Message = e.Message;
            }
            return response;
        }
        public async Task<Response<IEnumerable<EmployeeSalaryDTO>>> GetAllAsync()
        {
            var response = new Response<IEnumerable<EmployeeSalaryDTO>>();
            try
            {
                var employeeSalary = await _employeeSalaryDomain.GetAllAsync();
                response.Data = _mapper.Map<IEnumerable<EmployeeSalaryDTO>>(employeeSalary);

                if (response.Data != null)
                {
                    response.isSuccess = true;
                    response.Message = "Consulta exitosa";
                }
            }
            catch (Exception e)
            {
                response.Message = e.Message;
            }
            return response;
        }
        #endregion
    }
}
