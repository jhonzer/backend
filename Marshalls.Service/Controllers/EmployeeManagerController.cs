﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Marshalls.Service.Controllers
{
    public class EmployeeManagerController : Controller
    {
        // GET: EmployeeManagerController
        public ActionResult Index()
        {
            return View();
        }

        // GET: EmployeeManagerController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: EmployeeManagerController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: EmployeeManagerController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: EmployeeManagerController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: EmployeeManagerController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: EmployeeManagerController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: EmployeeManagerController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
