﻿using Marshalls.Domain.Entity;
using Marshalls.Domain.Interface;
using Marshalls.Infraestructure.Interface;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marshalls.Domain.Core
{
    public class EmployeeSalaryDomain : IEmployeeSalaryDomain
    {
        private readonly IEmployeeSalaryRepository _employeeSalaryRepository;

        public EmployeeSalaryDomain(IEmployeeSalaryRepository EmployeeSalaryRepository)
        {
            _employeeSalaryRepository = EmployeeSalaryRepository;
        }

        #region Métodos Síncronos
        public bool Insert(EmployeeSalary employeeSalary)
        {
            return _employeeSalaryRepository.Insert(employeeSalary);
        }
        public bool Update(EmployeeSalary employeeSalary)
        {
            return _employeeSalaryRepository.Update(employeeSalary);
        }

        public bool Delete(int Id)
        {
            return _employeeSalaryRepository.Delete(Id);
        }

        public EmployeeSalary Get(int Id)
        {
            return _employeeSalaryRepository.Get(Id);
        }

        public IEnumerable<EmployeeSalary> GetAll()
        {
            return _employeeSalaryRepository.GetAll();
        }
        #endregion

        #region Métodos Síncronos
        public async Task<bool> InsertAsync(EmployeeSalary employeeSalary)
        {
            return await _employeeSalaryRepository.InsertAsync(employeeSalary);
        }
        public async Task<bool> UpdateAsync(EmployeeSalary employeeSalary)
        {
            return await _employeeSalaryRepository.UpdateAsync(employeeSalary);
        }

        public async Task<bool> DeleteAsync(int Id)
        {
            return await _employeeSalaryRepository.DeleteAsync(Id);
        }

        public async Task<EmployeeSalary> GetAsync(int Id)
        {
            return await _employeeSalaryRepository.GetAsync(Id);
        }

        public async Task<IEnumerable<EmployeeSalary>> GetAllAsync()
        {
            return await _employeeSalaryRepository.GetAllAsync();
        }
        #endregion
    }
}
