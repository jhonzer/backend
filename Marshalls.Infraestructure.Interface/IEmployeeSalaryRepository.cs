﻿using Marshalls.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Marshalls.Infraestructure.Interface
{
    public interface IEmployeeSalaryRepository
    {
        #region Métodos síncronos
        bool Insert(EmployeeSalary employeeSalary);
        bool Update(EmployeeSalary employeeSalary);
        bool Delete(int Id);
        EmployeeSalary Get(int Id);
        IEnumerable<EmployeeSalary> GetAll();
        #endregion

        #region Métodos asíncronos
        Task<bool> InsertAsync(EmployeeSalary employeeSalary);
        Task<bool> UpdateAsync(EmployeeSalary employeeSalary);
        Task<bool> DeleteAsync(int Id);
        Task<EmployeeSalary> GetAsync(int Id);
        Task<IEnumerable<EmployeeSalary>> GetAllAsync();
        #endregion
    }
}
