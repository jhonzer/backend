﻿using Dapper;
using Marshalls.Domain.Entity;
using Marshalls.Infraestructure.Interface;
using Marshalls.Transversal.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Marshalls.Infraestructure.Repository
{
    public class EmployeeSalaryRepository : IEmployeeSalaryRepository
    {
        private readonly IConnectionFactory _connectionFactory;

        public EmployeeSalaryRepository(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        #region Métodos síncronos
        public bool Insert(EmployeeSalary employeeSalary)
        {
            using var connection = _connectionFactory.GetConnection;
            var query = "EmployeeSalaryInsert";
            var parameters = new DynamicParameters();
            parameters.Add("Id", employeeSalary.Id);
            parameters.Add("Year", employeeSalary.Year);
            parameters.Add("Month", employeeSalary.Month);
            parameters.Add("Office", employeeSalary.Office);
            parameters.Add("EmployeeCode", employeeSalary.EmployeeCode);
            parameters.Add("EmployeeName", employeeSalary.EmployeeName);
            parameters.Add("EmployeeSurname", employeeSalary.EmployeeSurname);
            parameters.Add("Division", employeeSalary.Division);
            parameters.Add("Position", employeeSalary.Position);
            parameters.Add("Grade", employeeSalary.Grade);
            parameters.Add("BeginDate", employeeSalary.BeginDate);
            parameters.Add("Birthday", employeeSalary.Birthday);
            parameters.Add("IdentificationNumber", employeeSalary.IdentificationNumber);
            parameters.Add("BaseSalary", employeeSalary.BaseSalary);
            parameters.Add("ProductionBonus", employeeSalary.ProductionBonus);
            parameters.Add("CompensationBonus", employeeSalary.CompensationBonus);
            parameters.Add("Commission", employeeSalary.Commission);
            parameters.Add("Contributions", employeeSalary.Contributions);

            var result = connection.Execute(query, param: parameters, commandType: CommandType.StoredProcedure);
            return result > 0;
        }

        public bool Update(EmployeeSalary employeeSalary)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "EmployeeSalaryUpdate";
                var parameters = new DynamicParameters();
                parameters.Add("Id", employeeSalary.Id);
                parameters.Add("Year", employeeSalary.Year);
                parameters.Add("Month", employeeSalary.Month);
                parameters.Add("Office", employeeSalary.Office);
                parameters.Add("EmployeeCode", employeeSalary.EmployeeCode);
                parameters.Add("EmployeeName", employeeSalary.EmployeeName);
                parameters.Add("EmployeeSurname", employeeSalary.EmployeeSurname);
                parameters.Add("Division", employeeSalary.Division);
                parameters.Add("Position", employeeSalary.Position);
                parameters.Add("Grade", employeeSalary.Grade);
                parameters.Add("BeginDate", employeeSalary.BeginDate);
                parameters.Add("Birthday", employeeSalary.Birthday);
                parameters.Add("IdentificationNumber", employeeSalary.IdentificationNumber);
                parameters.Add("BaseSalary", employeeSalary.BaseSalary);
                parameters.Add("ProductionBonus", employeeSalary.ProductionBonus);
                parameters.Add("CompensationBonus", employeeSalary.CompensationBonus);
                parameters.Add("Commission", employeeSalary.Commission);
                parameters.Add("Contributions", employeeSalary.Contributions);

                var result = connection.Execute(query, param: parameters, commandType: CommandType.StoredProcedure);
                return result > 0;
            }
        }

        public bool Delete(int Id)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "employeeSalaryDelete";
                var parameters = new DynamicParameters();
                parameters.Add("Id", Id);
                var result = connection.Execute(query, param: parameters, commandType: CommandType.StoredProcedure);
                return result > 0;
            }
        }

        public EmployeeSalary Get(int Id)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "CustomersGetByID";
                var parameters = new DynamicParameters();
                parameters.Add("Id", Id);
                var employeeSalary = connection.QuerySingle<EmployeeSalary>(query, param: parameters, commandType: CommandType.StoredProcedure);
                return employeeSalary;
            }
        }

        public IEnumerable<EmployeeSalary> GetAll()
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "CustomersList";

                var employeeSalary = connection.Query<EmployeeSalary>(query, commandType: CommandType.StoredProcedure);
                return employeeSalary;
            }
        }
        #endregion

        #region Métodos Asíncronos
        public async Task<bool> InsertAsync(EmployeeSalary employeeSalary)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "CustomersInsert";
                var parameters = new DynamicParameters();
                parameters.Add("Id", employeeSalary.Id);
                parameters.Add("Year", employeeSalary.Year);
                parameters.Add("Month", employeeSalary.Month);
                parameters.Add("Office", employeeSalary.Office);
                parameters.Add("EmployeeCode", employeeSalary.EmployeeCode);
                parameters.Add("EmployeeName", employeeSalary.EmployeeName);
                parameters.Add("EmployeeSurname", employeeSalary.EmployeeSurname);
                parameters.Add("Division", employeeSalary.Division);
                parameters.Add("Position", employeeSalary.Position);
                parameters.Add("Grade", employeeSalary.Grade);
                parameters.Add("BeginDate", employeeSalary.BeginDate);
                parameters.Add("Birthday", employeeSalary.Birthday);
                parameters.Add("IdentificationNumber", employeeSalary.IdentificationNumber);
                parameters.Add("BaseSalary", employeeSalary.BaseSalary);
                parameters.Add("ProductionBonus", employeeSalary.ProductionBonus);
                parameters.Add("CompensationBonus", employeeSalary.CompensationBonus);
                parameters.Add("Commission", employeeSalary.Commission);
                parameters.Add("Contributions", employeeSalary.Contributions);

                var result = await connection.ExecuteAsync(query, param: parameters, commandType: CommandType.StoredProcedure);
                return result > 0;
            }
        }

        public async Task<bool> UpdateAsync(EmployeeSalary employeeSalary)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "CustomersUpdate";
                var parameters = new DynamicParameters();
                parameters.Add("Id", employeeSalary.Id);
                parameters.Add("Year", employeeSalary.Year);
                parameters.Add("Month", employeeSalary.Month);
                parameters.Add("Office", employeeSalary.Office);
                parameters.Add("EmployeeCode", employeeSalary.EmployeeCode);
                parameters.Add("EmployeeName", employeeSalary.EmployeeName);
                parameters.Add("EmployeeSurname", employeeSalary.EmployeeSurname);
                parameters.Add("Division", employeeSalary.Division);
                parameters.Add("Position", employeeSalary.Position);
                parameters.Add("Grade", employeeSalary.Grade);
                parameters.Add("BeginDate", employeeSalary.BeginDate);
                parameters.Add("Birthday", employeeSalary.Birthday);
                parameters.Add("IdentificationNumber", employeeSalary.IdentificationNumber);
                parameters.Add("BaseSalary", employeeSalary.BaseSalary);
                parameters.Add("ProductionBonus", employeeSalary.ProductionBonus);
                parameters.Add("CompensationBonus", employeeSalary.CompensationBonus);
                parameters.Add("Commission", employeeSalary.Commission);
                parameters.Add("Contributions", employeeSalary.Contributions);

                var result = await connection.ExecuteAsync(query, param: parameters, commandType: CommandType.StoredProcedure);
                return result > 0;
            }
        }

        public async Task<bool> DeleteAsync(int Id)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "CustomersDelete";
                var parameters = new DynamicParameters();
                parameters.Add("Id", Id);
                var result = await connection.ExecuteAsync(query, param: parameters, commandType: CommandType.StoredProcedure);
                return result > 0;
            }
        }

        public async Task<EmployeeSalary> GetAsync(int Id)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "EmployeeSalaryGetByID";
                var parameters = new DynamicParameters();
                parameters.Add("Id", Id);
                var employeeSalary = await connection.QuerySingleAsync<EmployeeSalary>(query, param: parameters, commandType: CommandType.StoredProcedure);
                return employeeSalary;
            }
        }

        public async Task<IEnumerable<EmployeeSalary>> GetAllAsync()
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "EmployeeSalaryList";

                var employeeSalary = await connection.QueryAsync<EmployeeSalary>(query, commandType: CommandType.StoredProcedure);
                return employeeSalary;
            }
        }
        #endregion
    }
}
